import os

from celery import Celery
from core import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")

app = Celery('CELERY', broker=settings.CELERY_BROKER_URL)
app.conf.accept_content = ['application/json', 'application/x-python-serialize']
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()