from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Count, Case, When, IntegerField

from mail.tasks import handle_distributions
from mail.models import User, Message, Distribution
from mail.serializers import UserSerializer, MessageSerializer \
    , DistributionSerializer, DetailMessageStatisticsByDistributionSerializer


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class MessageViewSet(ModelViewSet): 
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

class DistributionViewSet(ModelViewSet):
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer

    def create(self, request, *args, **kwargs):
        """  
            Пример запроса, в поле filter должен передаваться dict который состоит пары ключей и значений
            Где значение это параметр по которому вы хотите отфильтровать. 

            Передавать значения в фильтре не обязательно!
            { "start": "2023-09-25 18:16:30",
                "end": "2023-09-25 18:17:00",
                "text": "string",
                "filter": {"tag": "tag3",  "code": "3"}
                }
            
            """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        handle_distributions.delay(serializer.data['id'])
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class DetailMessageStatisticsByDistributionView(ListAPIView):
    serializer_class = DetailMessageStatisticsByDistributionSerializer

    def get(self, requests, *args, **kwargs):
        message = Message.objects.filter(distribution__id=kwargs['pk']).select_related('distribution')
        serialized_data = self.get_serializer(message, many=True)
        return Response(serialized_data.data)



class DistributionStatistics(APIView):
    def get(self, request, *args, **kwargs):
        statistics = Distribution.objects.annotate(
            total_messages=Count('distribution'),
            sent_messages=Count(
                Case(
                    When(distribution__status=True, then=1),
                    default=None,
                    output_field=IntegerField()
                )
            ),
            unsent_messages=Count(
                Case(
                    When(distribution__status=False, then=1),
                    default=None,
                    output_field=IntegerField()
                )
            )).values('id', 'start', 'end', 'text', 'total_messages', 'sent_messages', 'unsent_messages')
        return Response(statistics)