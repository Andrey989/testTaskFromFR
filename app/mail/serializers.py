from rest_framework.serializers import ModelSerializer, DecimalField, CharField, PrimaryKeyRelatedField

from mail.models import User, Message, Distribution


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'



class MessageSerializer(ModelSerializer):
    distribution = PrimaryKeyRelatedField(queryset=Distribution.objects.all())
    user = PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = Message
        fields = '__all__'

class DistributionSerializer(ModelSerializer):
    class Meta:
        model = Distribution
        fields = '__all__'

class DetailMessageStatisticsByDistributionSerializer(ModelSerializer):
    distribution = DistributionSerializer()

    class Meta:
        model = Message
        fields = '__all__'

class StatisticSerializer(ModelSerializer):
    
    # def to_representation(self, instance):
    #     rep = super().to_representation(instance)
    #     # rep


    class Meta:
        model = Distribution
        fields = '__all__'