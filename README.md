# Способы запуска программмы 

+ Docker compose. Настоятельно рекомендую использовать этот метод
    ```
    docker compose up -d --build
    ```

+  Python
    ```
    python app/manage.py migrate
    python app/manage.py runserver
    ```
    Установка зависимостей:
    ```
    pip install -r app/requirements.txt
    ```

# Tasks

- [x] Добавления нового клиента в справочник со всеми его атрибутами
- [x] обновления данных атрибутов клиента
- [x] удаления клиента из справочника
- [x] добавления новой рассылки со всеми её атрибутами
- [x] получения общей статистики по созданным рассылкам и количеству отправленных сообщений по ним с группировкой по статусам
- [x] получения детальной статистики отправленных сообщений по конкретной рассылке
- [x] обновления атрибутов рассылки
- [x] удаления рассылки
- [x] обработки активных рассылок и отправки сообщений клиентам



# Additional Tasks
- [x] подготовить docker-compose для запуска всех сервисов проекта одной командой
- [x] сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
- [x] реализовать отдачу метрик в формате prometheus и задокументировать эндпоинты и экспортируемые метрики


