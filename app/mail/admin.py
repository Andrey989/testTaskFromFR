from django.contrib import admin

from mail.models import User, Distribution, Message
# Register your models here.

admin.site.register(User)
admin.site.register(Distribution)
admin.site.register(Message)