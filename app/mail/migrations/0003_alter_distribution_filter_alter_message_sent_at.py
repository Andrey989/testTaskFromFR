# Generated by Django 4.2.5 on 2023-09-24 18:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0002_alter_message_distribution'),
    ]

    operations = [
        migrations.AlterField(
            model_name='distribution',
            name='filter',
            field=models.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='sent_at',
            field=models.DateTimeField(default=None),
        ),
    ]
